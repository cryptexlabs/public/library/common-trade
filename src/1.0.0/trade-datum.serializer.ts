import {TradeSerializedInterface} from "./trade-serialized.interface";
import {TradeInterface} from "./trade.interface";
import {TradeSerializer} from "./trade.serializer";

export class TradeDatumSerializer {

    public readonly trades: TradeSerializedInterface[];

    constructor(trades: TradeInterface[]) {
        this.trades = [];
        for (const trade of trades) {
            this.trades.push(new TradeSerializer(trade));
        }
    }

    public getDatum(): TradeSerializedInterface[] {
        return this.trades;
    }
}