import {TradeInterface} from "./trade.interface";

export class TradeUtil {
    private constructor(props) {
    }

    public static getTradeIdsForList(trades: TradeInterface[]) {
        const tradeIds = [];
        for (const trade of trades) {
            tradeIds.push(trade.getTradeId());
        }
        return tradeIds;
    }

    public static getLastTradeTime(trades: TradeInterface[]){
        if (trades.length > 0) {
            return trades[0].getTime();
        } else {
            return -1;
        }
    }

}