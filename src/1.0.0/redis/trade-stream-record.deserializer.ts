import {TradeInterface} from "../trade.interface";
import {TradeDeserializer} from "../trade.deserializer";
import {TradeSchema} from "../trade-schema";

export class TradeStreamRecordDeserializer {

    private trade: TradeInterface;

    constructor(data: any[]) {
        this.trade = null;
        if (typeof data[0] === "undefined") {
            return;
        }
        if (typeof data[1] === "undefined") {
            return;
        }
        const rawRecordData = data[1];
        if (typeof rawRecordData[0] === "undefined") {
            return;
        }
        if (typeof rawRecordData[1] === "undefined") {
            return;
        }
        if (rawRecordData[0] !== TradeSchema.getTradeUpdateStreamKey()) {
            return;
        }
        const jsonTradeData = rawRecordData[1];
        if (jsonTradeData === "") {
            return;
        }

        const rawTrade = JSON.parse(jsonTradeData);

        this.trade = new TradeDeserializer(rawTrade);
    }

    public getTrade(): TradeInterface {
        return this.trade;
    }
}