import {TradeStreamRecordDeserializer} from "./trade-stream-record.deserializer";
import {TradeInterface} from "../trade.interface";

export class TradeStreamMessageDeserializer {

    private trades: TradeInterface[];

    constructor(message: any) {
        this.trades = [];
        if (!message) {
            return;
        }
        if (!Array.isArray(message)) {
            return;
        }
        if (typeof message[0] === "undefined") {
            return;
        }
        if (typeof message[0][1] === "undefined") {
            return;
        }
        if (typeof message[0][1][0] === "undefined") {
            return;
        }

        const rawTradeStream = message[0][1];

        for (const rawRecord of rawTradeStream) {
            const record = new TradeStreamRecordDeserializer(rawRecord);
            const trade  = record.getTrade();
            if (trade) {
                this.trades.push(record.getTrade());
            }
        }
    }

    public getTrades(): TradeInterface[] {
        return this.trades;
    }
}