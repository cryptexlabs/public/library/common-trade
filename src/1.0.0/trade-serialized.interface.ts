export class TradeSerializedInterface {
    time: number;
    price: string;
    quantity: string;
    tradeId: string;
}