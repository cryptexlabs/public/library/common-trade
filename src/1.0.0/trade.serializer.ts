import {TradeInterface} from "./trade.interface";

export class TradeSerializer {

    public readonly time: number;
    public readonly price: string;
    public readonly quantity: string;
    public readonly tradeId: string;

    constructor(trade: TradeInterface) {
        this.time     = trade.getTime();
        this.price    = trade.getPrice();
        this.quantity = trade.getQuantity();
        this.tradeId = trade.getTradeId();
    }
}