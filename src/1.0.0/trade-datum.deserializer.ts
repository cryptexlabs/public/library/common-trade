import {TradeSerializedInterface} from "./trade-serialized.interface";
import {TradeInterface} from "./trade.interface";
import {TradeDeserializer} from "./trade.deserializer";

export class TradeDatumDeserializer {

    private trades: TradeInterface[];

    constructor(trades: TradeSerializedInterface[]) {
        this.trades = [];
        for (const trade of trades) {
            this.trades.push(new TradeDeserializer(trade));
        }
    }

    public getDatum(): TradeInterface[] {
        return this.trades;
    }
}