import {TradeInterface} from "./trade.interface";
import {TradeSerializer} from "./trade.serializer";

export class TradeDeserializer implements TradeInterface {

    constructor(private readonly data: TradeSerializer) {
    }

    getTime(): number {
        return this.data.time;
    }

    getTradeId(): string {
        return this.data.tradeId;
    }

    getPrice(): string {
        return this.data.price;
    }

    getQuantity(): string {
        return this.data.quantity;
    }

}