import {TradeSchema} from "./trade-schema";

export class TradeIdGenerator {
    public static generateTradeId(
        exchangeId: string,
        pairId: string,
        provider: string,
        generator: string,
        price: string,
        quantity: string,
        time: number): string {

        const data     = {
            version: TradeSchema.getVersion(),
            exchangeId,
            pairId,
            provider,
            generator,
            price,
            quantity,
            time,
        };
        const jsonData = JSON.stringify(data);
        return Buffer.from(jsonData).toString("base64");
    }
}