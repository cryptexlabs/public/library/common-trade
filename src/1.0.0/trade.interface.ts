/**
 * Executed trades
 */
export interface TradeInterface {

    /**
     * Unix timestamp in milliseconds
     *
     * @return {number}
     */
    getTime(): number;

    /**
     * A unique identifier for the trade
     *
     * @return {string}
     */
    getTradeId(): string;

    /**
     *
     * @return {string}
     */
    getPrice(): string;

    /**
     * The quantity of the asset traded
     *
     * @return {string}
     */
    getQuantity(): string;

}