import path = require("path");

export class TradeSchema {

    private static readonly version = path.basename(__dirname);

    private constructor() {
    }

    public static getVersion(): string {
        return this.version;
    }

    public static getTradeUpdateMessageId(){
        return "trade";
    }

    /**
     * @deprecated Use getTradeUpdateMessageId and getVersion with stream library instead
     */
    public static getTradeUpdateStreamKey() {
        return this.getTradeUpdateMessageId() + "-" + this.getVersion();
    }
}