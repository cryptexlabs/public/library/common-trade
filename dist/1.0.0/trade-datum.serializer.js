"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const trade_serializer_1 = require("./trade.serializer");
class TradeDatumSerializer {
    constructor(trades) {
        this.trades = [];
        for (const trade of trades) {
            this.trades.push(new trade_serializer_1.TradeSerializer(trade));
        }
    }
    getDatum() {
        return this.trades;
    }
}
exports.TradeDatumSerializer = TradeDatumSerializer;
//# sourceMappingURL=trade-datum.serializer.js.map