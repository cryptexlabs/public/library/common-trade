"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const path = require("path");
class TradeSchema {
    constructor() {
    }
    static getVersion() {
        return this.version;
    }
    static getTradeUpdateMessageId() {
        return "trade";
    }
    static getTradeUpdateStreamKey() {
        return this.getTradeUpdateMessageId() + "-" + this.getVersion();
    }
}
TradeSchema.version = path.basename(__dirname);
exports.TradeSchema = TradeSchema;
//# sourceMappingURL=trade-schema.js.map