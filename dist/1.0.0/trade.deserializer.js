"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
class TradeDeserializer {
    constructor(data) {
        this.data = data;
    }
    getTime() {
        return this.data.time;
    }
    getTradeId() {
        return this.data.tradeId;
    }
    getPrice() {
        return this.data.price;
    }
    getQuantity() {
        return this.data.quantity;
    }
}
exports.TradeDeserializer = TradeDeserializer;
//# sourceMappingURL=trade.deserializer.js.map