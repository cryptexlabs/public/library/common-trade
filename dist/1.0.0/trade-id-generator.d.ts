export declare class TradeIdGenerator {
    static generateTradeId(exchangeId: string, pairId: string, provider: string, generator: string, price: string, quantity: string, time: number): string;
}
