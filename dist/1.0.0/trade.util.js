"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
class TradeUtil {
    constructor(props) {
    }
    static getTradeIdsForList(trades) {
        const tradeIds = [];
        for (const trade of trades) {
            tradeIds.push(trade.getTradeId());
        }
        return tradeIds;
    }
    static getLastTradeTime(trades) {
        if (trades.length > 0) {
            return trades[0].getTime();
        }
        else {
            return -1;
        }
    }
}
exports.TradeUtil = TradeUtil;
//# sourceMappingURL=trade.util.js.map