"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
class TradeSerializer {
    constructor(trade) {
        this.time = trade.getTime();
        this.price = trade.getPrice();
        this.quantity = trade.getQuantity();
        this.tradeId = trade.getTradeId();
    }
}
exports.TradeSerializer = TradeSerializer;
//# sourceMappingURL=trade.serializer.js.map