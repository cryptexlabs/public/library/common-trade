"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const trade_deserializer_1 = require("./trade.deserializer");
class TradeDatumDeserializer {
    constructor(trades) {
        this.trades = [];
        for (const trade of trades) {
            this.trades.push(new trade_deserializer_1.TradeDeserializer(trade));
        }
    }
    getDatum() {
        return this.trades;
    }
}
exports.TradeDatumDeserializer = TradeDatumDeserializer;
//# sourceMappingURL=trade-datum.deserializer.js.map