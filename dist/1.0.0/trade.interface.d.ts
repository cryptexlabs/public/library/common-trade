export interface TradeInterface {
    getTime(): number;
    getTradeId(): string;
    getPrice(): string;
    getQuantity(): string;
}
