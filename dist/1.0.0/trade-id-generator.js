"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const trade_schema_1 = require("./trade-schema");
class TradeIdGenerator {
    static generateTradeId(exchangeId, pairId, provider, generator, price, quantity, time) {
        const data = {
            version: trade_schema_1.TradeSchema.getVersion(),
            exchangeId,
            pairId,
            provider,
            generator,
            price,
            quantity,
            time,
        };
        const jsonData = JSON.stringify(data);
        return Buffer.from(jsonData).toString("base64");
    }
}
exports.TradeIdGenerator = TradeIdGenerator;
//# sourceMappingURL=trade-id-generator.js.map