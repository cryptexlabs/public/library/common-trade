export declare class TradeSchema {
    private static readonly version;
    private constructor();
    static getVersion(): string;
    static getTradeUpdateMessageId(): string;
    static getTradeUpdateStreamKey(): string;
}
