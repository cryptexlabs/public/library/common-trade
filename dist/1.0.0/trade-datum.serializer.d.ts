import { TradeSerializedInterface } from "./trade-serialized.interface";
import { TradeInterface } from "./trade.interface";
export declare class TradeDatumSerializer {
    readonly trades: TradeSerializedInterface[];
    constructor(trades: TradeInterface[]);
    getDatum(): TradeSerializedInterface[];
}
