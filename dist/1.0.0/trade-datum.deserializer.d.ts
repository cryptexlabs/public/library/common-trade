import { TradeSerializedInterface } from "./trade-serialized.interface";
import { TradeInterface } from "./trade.interface";
export declare class TradeDatumDeserializer {
    private trades;
    constructor(trades: TradeSerializedInterface[]);
    getDatum(): TradeInterface[];
}
