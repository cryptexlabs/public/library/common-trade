import { TradeInterface } from "../trade.interface";
export declare class TradeStreamMessageDeserializer {
    private trades;
    constructor(message: any);
    getTrades(): TradeInterface[];
}
