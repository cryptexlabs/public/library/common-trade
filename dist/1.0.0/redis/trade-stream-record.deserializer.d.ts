import { TradeInterface } from "../trade.interface";
export declare class TradeStreamRecordDeserializer {
    private trade;
    constructor(data: any[]);
    getTrade(): TradeInterface;
}
