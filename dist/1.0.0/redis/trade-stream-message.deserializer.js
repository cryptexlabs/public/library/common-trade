"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const trade_stream_record_deserializer_1 = require("./trade-stream-record.deserializer");
class TradeStreamMessageDeserializer {
    constructor(message) {
        this.trades = [];
        if (!message) {
            return;
        }
        if (!Array.isArray(message)) {
            return;
        }
        if (typeof message[0] === "undefined") {
            return;
        }
        if (typeof message[0][1] === "undefined") {
            return;
        }
        if (typeof message[0][1][0] === "undefined") {
            return;
        }
        const rawTradeStream = message[0][1];
        for (const rawRecord of rawTradeStream) {
            const record = new trade_stream_record_deserializer_1.TradeStreamRecordDeserializer(rawRecord);
            const trade = record.getTrade();
            if (trade) {
                this.trades.push(record.getTrade());
            }
        }
    }
    getTrades() {
        return this.trades;
    }
}
exports.TradeStreamMessageDeserializer = TradeStreamMessageDeserializer;
//# sourceMappingURL=trade-stream-message.deserializer.js.map