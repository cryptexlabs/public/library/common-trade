"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const trade_deserializer_1 = require("../trade.deserializer");
const trade_schema_1 = require("../trade-schema");
class TradeStreamRecordDeserializer {
    constructor(data) {
        this.trade = null;
        if (typeof data[0] === "undefined") {
            return;
        }
        if (typeof data[1] === "undefined") {
            return;
        }
        const rawRecordData = data[1];
        if (typeof rawRecordData[0] === "undefined") {
            return;
        }
        if (typeof rawRecordData[1] === "undefined") {
            return;
        }
        if (rawRecordData[0] !== trade_schema_1.TradeSchema.getTradeUpdateStreamKey()) {
            return;
        }
        const jsonTradeData = rawRecordData[1];
        if (jsonTradeData === "") {
            return;
        }
        const rawTrade = JSON.parse(jsonTradeData);
        this.trade = new trade_deserializer_1.TradeDeserializer(rawTrade);
    }
    getTrade() {
        return this.trade;
    }
}
exports.TradeStreamRecordDeserializer = TradeStreamRecordDeserializer;
//# sourceMappingURL=trade-stream-record.deserializer.js.map