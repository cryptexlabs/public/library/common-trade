import { TradeInterface } from "./trade.interface";
import { TradeSerializer } from "./trade.serializer";
export declare class TradeDeserializer implements TradeInterface {
    private readonly data;
    constructor(data: TradeSerializer);
    getTime(): number;
    getTradeId(): string;
    getPrice(): string;
    getQuantity(): string;
}
