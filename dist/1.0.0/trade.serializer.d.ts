import { TradeInterface } from "./trade.interface";
export declare class TradeSerializer {
    readonly time: number;
    readonly price: string;
    readonly quantity: string;
    readonly tradeId: string;
    constructor(trade: TradeInterface);
}
