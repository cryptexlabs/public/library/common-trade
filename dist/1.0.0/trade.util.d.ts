import { TradeInterface } from "./trade.interface";
export declare class TradeUtil {
    private constructor();
    static getTradeIdsForList(trades: TradeInterface[]): any[];
    static getLastTradeTime(trades: TradeInterface[]): number;
}
